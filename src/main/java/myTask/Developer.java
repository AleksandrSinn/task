package main.java.myTask;

import java.util.Objects;

public class Developer extends Employee{
    private DevLanguage devLanguage;
    private String university;
    private Ide ide;

    public Developer(String name, String surname, double salary, PositionEnum positionEnum, Ide ide, DevLanguage devLanguage, String university) {
        super(name, surname, salary, positionEnum);
        this.devLanguage = devLanguage;
        this.ide = ide;
        this.university = university;
    }

    @Override
    protected String getAdditionalInfo() {
        return "DevLanguage = " + devLanguage + "; IDE = " + ide + "; University = " + university;
    }

    @Override
    public int compareTo(Employee o) {
        return (int) (this.getSalary() - o.getSalary());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Developer developer = (Developer) o;
        return devLanguage == developer.devLanguage && Objects.equals(university, developer.university) && ide == developer.ide;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), devLanguage, university, ide);
    }
}
