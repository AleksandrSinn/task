package main.java.myTask;

public enum DevLanguage {
    JAVA,
    PHP,
    JAVASCRIPT,
    SCALA;
}
