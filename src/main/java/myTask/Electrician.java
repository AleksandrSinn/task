package main.java.myTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Electrician extends Employee{
    private List<String> newtonsLaws = new ArrayList<>();
    private int countOfLaws;

    public Electrician(String name, String surname, double salary,  PositionEnum positionEnum, int countOfLaws) {
        super(name, surname, salary, positionEnum);
        try {
            if (countOfLaws < 1 && countOfLaws > 3)
                throw new Exception("Law number is not correct. Please choose 1, 2, or 3.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.countOfLaws = countOfLaws;

        newtonsLaws.add("Law 1. A body continues in its state of rest, or in uniform motion in a straight line, unless acted upon by a force.");
        newtonsLaws.add("Law 2. A body acted upon by a force moves in such a manner that the time rate of change of momentum equals the force.");
        newtonsLaws.add("Law 3. If two bodies exert forces on each other, these forces are equal in magnitude and opposite in direction.");
    }

    @Override
    protected String getAdditionalInfo() {
        return "Newton's Law = " + newtonsLaws.get(countOfLaws - 1);
    }

    @Override
    public int compareTo(Employee o) {
        return (int) (this.getSalary() - o.getSalary());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Electrician that = (Electrician) o;
        return countOfLaws == that.countOfLaws && Objects.equals(newtonsLaws, that.newtonsLaws);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), newtonsLaws, countOfLaws);
    }
}
