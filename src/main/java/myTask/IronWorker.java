package main.java.myTask;

import java.util.Objects;

public class IronWorker extends Employee{
    private DepartmentOfIronWorker department;

    public IronWorker(String name, String surname, double salary, PositionEnum positionEnum, DepartmentOfIronWorker department) {
        super(name, surname, salary, positionEnum);
        this.department = department;
    }

    @Override
    protected String getAdditionalInfo() {
        return "office = " + department;
    }


    @Override
    public int compareTo(Employee o) {
        return (int) (this.getSalary() - o.getSalary());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        IronWorker that = (IronWorker) o;
        return department == that.department;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), department);
    }
}
