package main.java.myTask;

import java.util.Objects;

public abstract class Employee implements Comparable<Employee>{
    private String name;
    private String surname;
    private double salary;
    private PositionEnum position;
    private Employee employee;

    public Employee(String name, String surname, double salary, PositionEnum position) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
        this.position = position;
    }

    protected  abstract String getAdditionalInfo();

    public PositionEnum getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary=" + salary +
                ", position=" + position + ","
                + getAdditionalInfo() + "}";

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee1 = (Employee) o;
        return Double.compare(employee1.salary, salary) == 0 && Objects.equals(name, employee1.name) && Objects.equals(surname, employee1.surname) && position == employee1.position && Objects.equals(employee, employee1.employee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, salary, position, employee);
    }
}