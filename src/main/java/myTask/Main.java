package main.java.myTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Employee worker1 = new IronWorker("Ivan", "Ivanov", 1200, PositionEnum.IRONWORKER, DepartmentOfIronWorker.FACTORY);
        Employee worker2 = new Electrician("Albert", "Stolz", 1700, PositionEnum.ELECTRICIAN, 1);
        Employee worker3 = new Developer("Angelina", "Stepanova", 4300, PositionEnum.DEVELOPER, Ide.ECLIPSE, DevLanguage.JAVA, "KAI");
        Employee worker4 = new Developer("Marta", "Igoreva", 2300, PositionEnum.DEVELOPER, Ide.INTELLIJIDEA, DevLanguage.JAVA, "MGU");
        Employee worker5 = new Developer("Constantin", "Iliin", 550, PositionEnum.DEVELOPER, Ide.NETBEANS, DevLanguage.JAVASCRIPT, "KAI");

        List<Employee> employeeList = new ArrayList<>();

        employeeList.add(worker1);
        employeeList.add(worker2);
        employeeList.add(worker3);
        employeeList.add(worker4);
        employeeList.add(worker5);


        employeeList.forEach(System.out::println);

        System.out.println("-----------------------");

        employeeList.forEach(list-> System.out.println(list.getSurname() + " " + list.getName() + " : " + list.getSalary()));
    }
}
